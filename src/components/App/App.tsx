import React, { FunctionComponent } from "react";
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";
import LogicBody from "../LogicBody/LogicBody";
import Game from "../../models/Game";

const App: FunctionComponent = () => {
  const game: Game = new Game();
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-title">PokerStats</h1>
        <div className="hearder-margin"></div>
        <h2> by Guillaume Chafiol </h2>
      </header>
      <div className="App-MainAndDataPlayer ">
        <LogicBody game={game} />
      </div>
    </div>
  );
};

export default App;
