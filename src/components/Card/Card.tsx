import React, { FunctionComponent, useState, useEffect } from "react";
import "./Card.scss";
import cn from "classnames";
import CardManager from "../../models/CardManager";
import { CardValue } from "../../models/Enums";
import { PositionCard, NewDisplayedCard } from "../LogicBody/LogicBody";

type AppProps = {
  activate: boolean;
  openTooltip: (cardComponent: PositionCard) => void;
  position: PositionCard;
  newDisplayedCard: NewDisplayedCard;
};

const Card: FunctionComponent<AppProps> = (props) => {
  const [card, setCard] = useState<CardManager>(new CardManager());
  const [activate, setActivate] = useState<boolean>(props.activate!);

  useEffect(() => {
    setActivate(props.activate!);
  }, [props.activate]);

  useEffect(() => {
    if (
      props.newDisplayedCard.position.element === props.position.element &&
      props.newDisplayedCard.position.indexPlayer === props.position.indexPlayer &&
      props.newDisplayedCard.position.position === props.position.position
    )
      setCard(props.newDisplayedCard.newCard);
  }, [props.newDisplayedCard.newCard]);

  const defaultDisplay = card?.value === CardValue.none;
  const sourceSymbol = require("../../assets/pictures/" + card?.symbol + ".png");
  const sourceValue = require("../../assets/pictures/" + card?.value + ".png");

  return (
    <div className={cn("cardDiv", { activatedFalse: !activate })}>
      <button
        disabled={!activate}
        className={cn("card card_component ")}
        onClick={() => {
          props.openTooltip(props.position);
        }}
      >
        {!defaultDisplay && <img src={sourceSymbol} className="symbolCardImg" alt="symbolImg" />}
        <img src={sourceValue} className={cn("card_player", { card_img: !defaultDisplay })} alt="valueImg" />
      </button>
    </div>
  );
};

export default Card;
