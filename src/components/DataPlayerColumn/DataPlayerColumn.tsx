import React, { FunctionComponent } from "react";
import PlayerFrame from "../PlayerFrame/PlayerFrame";
import PlayerC from "../../models/PlayerManager";
import "./DataPlayerColumn.scss";
import { ActivatePlayer } from "../LogicBody/LogicBody";

type AppProps = {
  players: PlayerC[];
  activatedPlayerIds: ActivatePlayer;
  statisticDisplays: string[];
};

const DataPlayerColumn: FunctionComponent<AppProps> = ({ players, activatedPlayerIds, statisticDisplays }) => {
  return (
    <div className="dataPlayerColumn">
      <PlayerFrame
        player={players[0]}
        activatedPlayerIds={activatedPlayerIds}
        statistic={statisticDisplays[0] === undefined ? "" : statisticDisplays[0]}
      />
      <PlayerFrame
        player={players[1]}
        activatedPlayerIds={activatedPlayerIds}
        statistic={statisticDisplays[1] === undefined ? "" : statisticDisplays[1]}
      />
      <PlayerFrame
        player={players[2]}
        activatedPlayerIds={activatedPlayerIds}
        statistic={statisticDisplays[2] === undefined ? "" : statisticDisplays[2]}
      />
      <PlayerFrame
        player={players[3]}
        activatedPlayerIds={activatedPlayerIds}
        statistic={statisticDisplays[3] === undefined ? "" : statisticDisplays[3]}
      />
    </div>
  );
};

export default DataPlayerColumn;
