import React, { FunctionComponent, useState } from "react";
import background from "../../assets/pictures/table-spun.jpg";
import Player from "../Player/Player";
import SplitPot from "../SplitPot/SplitPot";
import Game from "../../models/Game";
import Table from "../Table/Table";
import cn from "classnames";
import "./Main.scss";
import { CardValue } from "../../models/Enums";
import { ActivatePlayer, SetActivePlayer, PositionCard, NewDisplayedCard } from "../LogicBody/LogicBody";
import axios from "axios";
import PlayerManager from "../../models/PlayerManager";
import "bootstrap/dist/js/bootstrap.bundle.min";

type AppProps = {
  game: Game;
  activatedPlayerIds: ActivatePlayer;
  setActivedPlayerIds: SetActivePlayer;
  openTooltip: (cardComponent: PositionCard) => void;
  newDisplayedCard: NewDisplayedCard;
  setStatisticDisplay: (statisticDisplay: string[]) => void;
};

const Main: FunctionComponent<AppProps> = (props: any) => {
  const [splitScore, setSplitScore] = useState<string>("   ");
  const [precision, setPrecision] = useState<number>(props.game.precision);

  const playerPositionning: object[] = [
    { left: 105, top: 148 },
    { left: 17, top: 344 },
    { left: 105, top: 526 },
    { left: 279, top: 559 },
    { left: 489, top: 559 },
    { left: 672, top: 519 },
    { left: 741, top: 343 },
    { left: 646, top: 148 }
  ];

  const precisionItems = [
    { value: 2, label: "70%", fullLabel: "calculation is instant, the precision can be wrong up to 30% maximum" },
    {
      value: 3,
      label: "80%",
      fullLabel: "calculation is almost instant, the precision can be wrong up to 20% maximum"
    },
    { value: 4, label: "90%", fullLabel: "calculation may take until 5 seconds, the precision can be wrong up to 10%" },
    { value: 5, label: "100%", fullLabel: "calculation may take until 30 second, the precision is perfect" }
  ];

  const enableRefresh = (): boolean => {
    const activPlayers: PlayerManager[] = props.game.players.filter((player: PlayerManager) => {
      return player.activ;
    });
    const unCheckPlayers: PlayerManager[] = props.game.players.filter((player: PlayerManager) => {
      return player.activ && (player.card1.value === CardValue.none || player.card2.value === CardValue.none);
    });
    return activPlayers.length > 0 && unCheckPlayers.length === 0;
  };

  const refresh = () => {
    axios.post("http://localhost:5000", props.game.reformateObject()).then(
      (res) => {
        const displays: string[] = [];
        props.game.players.forEach((player: PlayerManager) => {
          const statPlayer = res.data.players.find((statPlayer: any) => statPlayer.id === player.id);
          const display = statPlayer === undefined ? "" : statPlayer.score + " %";
          displays.push(display);
        });
        setSplitScore(res.data.splitScore + " %");
        props.setStatisticDisplay(displays);
      },
      (error) => {
        const displays: string[] = [];
        props.game.players.forEach((player: PlayerManager) => {
          const display = player.activ ? "???" : "";
          displays.push(display);
        });
        setSplitScore("");
        props.setStatisticDisplay(displays);
      }
    );
  };

  const updtePrecision = (precision: number) => {
    setPrecision(precision);
    props.game.precision = precision;
  };

  return (
    <div className="Main">
      <img className="background" src={background} alt="backgroundTable" />
      <SplitPot value={splitScore} />
      {props.game.players.map((player: PlayerManager, index: number) => {
        return (
          <Player
            player={player}
            position={playerPositionning[index]}
            activatedPlayerIds={props.activatedPlayerIds}
            setActivedPlayerIds={props.setActivedPlayerIds}
            openTooltip={props.openTooltip}
            newDisplayedCard={props.newDisplayedCard}
          />
        );
      })}
      <Table table={props.game.table} openTooltip={props.openTooltip} newDisplayedCard={props.newDisplayedCard} />
      <button
        className={cn("btn btn-lg btn-refresh", { "btn-primary": enableRefresh() }, { "btn-danger": !enableRefresh() })}
        disabled={!enableRefresh()}
        onClick={refresh}
      >
        Refresh
      </button>
      <div className="precision-box">
        <div className="btn-group">
          <button
            type="button"
            className="btn btn-secondary dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Precision
          </button>
          <div className="dropdown-menu">
            {precisionItems.map((item) => {
              return (
                <button
                  className={cn("dropdown-item", { precisionSelected: precision === item.value })}
                  onClick={() => updtePrecision(item.value)}
                >
                  {item.label}
                </button>
              );
            })}
            <div className="dropdown-divider"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
