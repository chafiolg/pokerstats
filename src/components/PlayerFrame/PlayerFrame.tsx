import React, { FunctionComponent, useState } from "react";
import Player from "../../models/PlayerManager";
import { Finality } from "../../models/Enums";
import cn from "classnames";
import "./PlayerFrame.scss";
import { ActivatePlayer } from "../LogicBody/LogicBody";

type AppProps = {
  player: Player;
  activatedPlayerIds: ActivatePlayer;
  statistic: string;
};

const PlayerFrame: FunctionComponent<AppProps> = ({ player, activatedPlayerIds, statistic }) => {
  const [finality] = useState<Finality>(player.finality);

  const getBasicStyle = () => {
    const number = player.id;
    const height = 151.9;
    let factor;
    let left;
    if (number < 5) {
      factor = number - 1;
      left = -110;
    } else {
      factor = Math.abs(number - 8);
      left = 911;
    }
    return { top: factor * height + 74.4, left: left, height: height };
  };
  /*const getNextFinality = () => {
    const final = finality;
    if (final === Finality.Win) {
      setFinality(Finality.Finish);
      player.finality = Finality.Finish;
    } else if (final === Finality.Finish) {
      setFinality(Finality.Lose);
      player.finality = Finality.Lose;
    } else if (final === Finality.Lose) {
      setFinality(Finality.Win);
      player.finality = Finality.Win;
    }
  };*/
  const changeFinishCombianson = (e: any) => {
    player.finishCombination = e.target.value;
  };

  const activ = activatedPlayerIds[player.id];
  const buttonFinalityClass = "playerFinalityButton " + finality;
  const selectorClass = "finishCombination " + finality;

  return (
    <div className={cn("Frame", finality, { activatedFalse: !activ })} style={getBasicStyle()}>
      <p className="namePlayer"> Player {player.id} </p>
      <button className={buttonFinalityClass} /*onClick={() => getNextFinality()}*/ disabled={!activ}>
        {" "}
      </button>
      {/*<button className="openOutsButton" style={this.getButtonOutsStyle()} disabled={!activate}> </button>*/}
      <label className="combinationLabel"> With : </label>
      <select
        disabled={true}
        className={selectorClass}
        onChange={(e) => {
          changeFinishCombianson(e);
        }}
        /*disabled={!activ}*/
      >
        <option className="combination"> No matter</option>
        <option className="combination"> High card</option>
        <option className="combination"> One pair</option>
        <option className="combination"> Two pair</option>
        <option className="combination"> Trips</option>
        <option className="combination"> Straight</option>
        <option className="combination"> Flush</option>
        <option className="combination"> Full</option>
        <option className="combination"> Quads</option>
        <option className="combination"> Straight Flush</option>
      </select>
      <p className="statNumber"> {statistic} </p>
    </div>
  );
};

export default PlayerFrame;
