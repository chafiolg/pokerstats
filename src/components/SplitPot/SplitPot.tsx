import React, { FunctionComponent } from "react";
import "./SplitPot.scss";

type AppProps = {
  value: string;
};

const SplitPot: FunctionComponent<AppProps> = ({ value }) => {
  return (
    <div className="splitPotBox surrondedText">
      <h2 className="title"> Split pot :</h2>
      <h2 className="stats"> {value} </h2>
      <div> </div>
    </div>
  );
};

export default SplitPot;
