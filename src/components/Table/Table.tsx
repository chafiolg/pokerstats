import React, { FunctionComponent, useState } from "react";
import cn from "classnames";
import "./Table.scss";
import Card from "../Card/Card";
import { PositionCard, NewDisplayedCard } from "../LogicBody/LogicBody";
import TableManager from "../../models/TableManager";

type AppProps = {
  openTooltip: (cardComponent: PositionCard) => void;
  newDisplayedCard: NewDisplayedCard;
  table: TableManager;
};
const Table: FunctionComponent<AppProps> = (props) => {
  const [openCardNumber, setOpenCardNumber] = useState<number>(5);

  const updateOpenCardNumber = (e: any) => {
    const openingCardNumber = parseInt(e.target.attributes["data-openingcard"].value);
    setOpenCardNumber(openingCardNumber);
    props.table.numberActivateCard = openingCardNumber;
  };

  const cardArrayActivation: boolean[] = [true, true, true, openCardNumber > 3, openCardNumber > 4];

  return (
    <div className="table">
      <Card
        activate={cardArrayActivation[0]}
        openTooltip={props.openTooltip}
        position={{ element: "Table", indexPlayer: -1, position: 0 }}
        newDisplayedCard={props.newDisplayedCard}
      />
      <Card
        activate={cardArrayActivation[1]}
        openTooltip={props.openTooltip}
        position={{ element: "Table", indexPlayer: -1, position: 1 }}
        newDisplayedCard={props.newDisplayedCard}
      />
      <Card
        activate={cardArrayActivation[2]}
        openTooltip={props.openTooltip}
        position={{ element: "Table", indexPlayer: -1, position: 2 }}
        newDisplayedCard={props.newDisplayedCard}
      />
      <div
        data-openingcard="3"
        className={cn("selectButton", { activatedFalse: openCardNumber === 3 })}
        style={{ left: 112 }}
        onClick={(e) => {
          updateOpenCardNumber(e);
        }}
      >
        {" "}
      </div>
      <div className="marge"> </div>
      <Card
        activate={cardArrayActivation[3]}
        openTooltip={props.openTooltip}
        position={{ element: "Table", indexPlayer: -1, position: 3 }}
        newDisplayedCard={props.newDisplayedCard}
      />
      <div
        data-openingcard="4"
        className={cn("selectButton", { activatedFalse: openCardNumber === 4 })}
        style={{ left: 167 }}
        onClick={(e) => {
          updateOpenCardNumber(e);
        }}
      >
        {" "}
      </div>
      <div className="marge"> </div>
      <Card
        activate={cardArrayActivation[4]}
        openTooltip={props.openTooltip}
        position={{ element: "Table", indexPlayer: -1, position: 4 }}
        newDisplayedCard={props.newDisplayedCard}
      />
      <div
        data-openingcard="5"
        className={cn("selectButton", { activatedFalse: openCardNumber === 5 })}
        style={{ left: 223 }}
        onClick={(e) => {
          updateOpenCardNumber(e);
        }}
      >
        {" "}
      </div>
    </div>
  );
};

export default Table;
