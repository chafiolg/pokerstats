import { Symbol, CardValue } from "./Enums";

export default class CardManager {
  value: CardValue | string;
  symbol: Symbol | string;

  constructor(symbol: Symbol | string = Symbol.Club, value: CardValue | string = CardValue.none) {
    this.value = value;
    this.symbol = symbol;
  }

  resetCard(value: CardValue, symbol: Symbol) {
    this.value = value;
    this.symbol = symbol;
  }

  equals(card: CardManager) {
    return card.value === this.value && card.symbol === this.symbol;
  }
}
