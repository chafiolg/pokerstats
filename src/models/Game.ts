import PlayerManager from "./PlayerManager";
import CardManager from "./CardManager";
import { Symbol, CardValue } from "./Enums";
import Table from "./TableManager";
import { NewDisplayedCard } from "../components/LogicBody/LogicBody";
import TableManager from "./TableManager";

export default class Game {
  nbPlayer: number = 8;
  players: PlayerManager[] = [];
  table: TableManager;
  splitPot: number;
  precision: number;

  constructor() {
    for (let i = 0; i < this.nbPlayer; i++) {
      this.players.push(new PlayerManager(i));
    }
    this.precision = 4; //number of maximum recursion made by server
    this.table = new Table();
    this.splitPot = 0; //percentage of statistic where the table win or at least 2 players wins
  }

  // call all the cards remaining in the deck and return an Array of this cards
  freeCardPicker = (): CardManager[] => {
    let cards: CardManager[] = [];
    for (let item of Object.values(Symbol)) {
      for (let cardValue of Object.values(CardValue)) {
        if (cardValue !== CardValue.none) cards.push(new CardManager(item, cardValue));
      }
    }

    for (let player of this.players) {
      cards = this.removeCard(cards, player.card1);
      cards = this.removeCard(cards, player.card2);
    }
    for (let card of this.table.cardsTable) {
      cards = this.removeCard(cards, card);
    }
    return cards;
  };

  pickedCards = (): CardManager[] => {
    let cards: CardManager[] = [];
    for (let player of this.players) {
      if (player.card1.value !== CardValue.none) cards.push(player.card1);
      if (player.card2.value !== CardValue.none) cards.push(player.card2);
    }
    for (let card of this.table.cardsTable) {
      if (card.value !== CardValue.none) cards.push(card);
    }
    return cards;
  };
  // remove a card from an array of card
  removeCard(cardTable: CardManager[], card: CardManager) {
    let copyTable = cardTable.slice(0, cardTable.length);
    let index = copyTable.findIndex((element) => element.equals(card));

    if (index !== -1) {
      copyTable.splice(index, 1);
    }

    return copyTable;
  }

  updateCardInGame(card: NewDisplayedCard) {
    if (card.position.element === "Table") {
      this.table.cardsTable[card.position.position] = card.newCard;
    } else if (card.position.element === "Player") {
      const indexCard: "card1" | "card2" = card.position.position === 0 ? "card1" : "card2";
      this.players[card.position.indexPlayer][indexCard] = card.newCard;
    }
  }

  reformateObject() {
    return {
      precision: this.precision,
      players: this.players.filter((player) => player.activ),
      table: this.table,
      freeCards: this.freeCardPicker()
    };
  }
}
