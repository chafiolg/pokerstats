import CardManager from "./CardManager";
//import Calculation from './Calculation.js';
import { Finality } from "./Enums";

export default class PlayerManager {
  id: number;
  card1: CardManager;
  card2: CardManager;
  finality: Finality = Finality.Win;
  finishCombination: string = "no matter";
  trueStatistic: number;
  display: string = "";
  activ: boolean = false;

  constructor(id: number) {
    this.id = id;
    this.card1 = new CardManager();
    this.card2 = new CardManager();
    this.trueStatistic = 0;
  }
}
